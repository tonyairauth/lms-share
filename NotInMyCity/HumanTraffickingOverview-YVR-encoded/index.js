getSCORMApi()
  .then(addPostMessageEventListener)
  .then(getIframeSrc)
  .then(createIframe)
  .catch(errorFunction);

function getSCORMApi() {
  return new Promise(function (resolve, reject) {
    var api = document._SCORM.initialize();
    if (api) {
      resolve(api);
    }
    reject('Error: SCORM failed to load');
  });
}

function addPostMessageEventListener(api) {
  return new Promise(function (resolve, reject) {
    if (!document._ecoConfig) {
      reject('Error: No configuration is defined');
    }
    if (!document._ecoConfig.url) {
      reject('Error: Origin is not defined in configuration');
    }
    window.addEventListener('message', function (event) {
      onMessage(event, api);
    }, false);
    resolve(api);
  })
}

function getIframeSrc(api) {
  return new Promise(function (resolve, reject) {
    var groupId = document._ecoConfig.id;
    if (!groupId) {
      reject('Error: Group Id is not defined in configuration');
    }
    var url = document._ecoConfig.url;
    if (!url) {
      reject('Error: Url is not defined in configuration')
    }
    var studentId = api.LMSGetValue('cmi.core.student_id');
    if (!studentId) {
      reject('Error: cmi.core.student_id is not defined');
    }
    var studentName = api.LMSGetValue('cmi.core.student_name');
    if (!studentName) {
      reject('Error: cmi.core.student_name is not defined');
    }
    var relativePath = '/includes/modules/FM_Guidebook/package_launcher/index.html';
    var userIdParam = '?userId=' + encodeURIComponent(studentId);
    var userNameParam = '&userName=' + encodeURIComponent(studentName);
    var groupIdParam = '&groupId=' + groupId;
    var sessionId = getSessionId(api);
    var sessionIdParam = '&sessionId=' + sessionId;
    var lessonLocation = api.LMSGetValue('cmi.core.lesson_location');
    var lessonLocationParam = '&lessonLocation=' + encodeURIComponent(lessonLocation);
    var src = url + relativePath + userIdParam + userNameParam + groupIdParam + sessionIdParam + lessonLocationParam;
    resolve(src);
  })
}

function createIframe(src) {
  var iframe = document.createElement('iframe');
  iframe.id = 'eco-launcher-iframe';
  iframe.setAttribute('src', src);
  document.body.replaceChild(iframe, document.getElementById('root'));
}

function errorFunction(errorMessage) {
  var header = document.createElement('h2');
  header.innerText = errorMessage;
  document.body.replaceChild(header, document.getElementById('root'));
}

function onMessage(event, api) {
  if (api && event.origin === document._ecoConfig.url) {
    getMessageData(event, api)
      .then(handleMessage)
      .then(commitChanges)
      .catch(onMessageError);
  }
}

function getMessageData(event, api) {
  return new Promise(function (resolve) {
    if (event.data.type) {
      resolve({api: api, type: event.data.type, payload: event.data.payload});
    } else {
      resolve({api: api, type:'UNKNOWN', payload: null})
    }
  })
}

function handleMessage(data) {
  switch (data.type) {
    case 'GRADE':
      return setGrade(data);
    case 'LOCATION':
      return setLocation(data);
    case 'SESSION_ID':
      return setSessionId(data);
    case 'START_TIMER':
      document._SessionTimer.start(data.api);
      return Promise.resolve(data);
    default:
      return Promise.resolve(data);
  }
}

function commitChanges(data) {
  return new Promise(function (resolve, reject) {
    var hasCommitted = data.api.LMSCommit('');
    if (hasCommitted !== 'true') {
      reject('Error: LMSCommit has failed for ' + data.type);
    }
    resolve()
  });
}

function onMessageError(errorMessage) {
  if (confirm(errorMessage)) {
    var header = document.createElement('h2');
    header.innerText = errorMessage;
    document.body.replaceChild(header, document.getElementById('eco-launcher-iframe'));
  }
}

function setGrade(data) {
  return new Promise(function (resolve, reject) {
    var hasSetValue;
    if (data.payload.completedTrackingCount === data.payload.trackingCount) {
      data.api.LMSSetValue("cmi.core.score.raw", "100");
      hasSetValue = data.api.LMSSetValue('cmi.core.lesson_status', 'completed');
    } else {
      hasSetValue = data.api.LMSSetValue('cmi.core.lesson_status', 'incomplete');
    }
    if (hasSetValue !== 'true') {
      reject('Error: LMSSetValue has failed for cmi.core.lesson_location');
    }
    resolve(data);
  })
}

function setLocation(data) {
  return new Promise(function (resolve, reject) {
    var hasSetValue = data.api.LMSSetValue('cmi.core.lesson_location', data.payload);
    if (hasSetValue !== 'true') {
      reject('Error: LMSSetValue has failed for cmi.core.lesson_location');
    }
    resolve(data);
  })
}

function setSessionId(data) {
  return new Promise(function (resolve, reject) {
    var suspendDataJson = JSON.stringify({sessionId: data.payload});
    var suspendData = btoa(suspendDataJson);
    var hasSetValue = data.api.LMSSetValue('cmi.suspend_data', suspendData);
    if (hasSetValue !== 'true') {
      reject('Error: LMSSetValue has failed for cmi.suspend_data');
    }
    resolve(data);
  });
}

function getSessionId(api) {
  var suspendData = getSuspendData(api);
  return suspendData && suspendData.sessionId ? suspendData.sessionId : '';
}

function getSuspendData(api) {
  var suspendDataB64 = api.LMSGetValue('cmi.suspend_data');
  var suspendData = atob(suspendDataB64);
  return isJSONObjectString(suspendData) ? JSON.parse(suspendData) : {};
}

function getSessionId(api) {
  var suspendData = getSuspendData(api);
  return suspendData && suspendData.sessionId ? suspendData.sessionId : '';
}

function isJSONObjectString(str) {
  try {
    var obj = JSON.parse(str);
    return obj === Object(obj);
  } catch (e) {
    return false;
  }
}

window.onbeforeunload = function () {
  document._SessionTimer.end();
  document._SCORM.finish();
};
