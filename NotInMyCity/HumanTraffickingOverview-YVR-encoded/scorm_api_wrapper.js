document._SCORM = {};

document._SCORM.initialize = function () {
  var api = document._SCORM.getHandle();
  if (api) {
    var hasInitialized = api.LMSInitialize('');
    if (hasInitialized === 'true') {
      document._SCORM.initializeLessonStatus(api);
      return api;
    }
  }
  return null;
};

document._SCORM.getHandle = function () {
  if (!document._SCORM.API) {
    document._SCORM.API = document._SCORM.getAPI();
  }
  return document._SCORM.API;
};

document._SCORM.getAPI = function () {
  return document._SCORM.findAPIFromWindow(window) ||
    document._SCORM.findAPIFromWindow(window.top.opener) ||
    document._SCORM.findAPIFromWindow(window.top.opener.document);
};

document._SCORM.findAPIFromWindow = function (win) {
  return document._SCORM.findAPI(win, 0, 7)
};

document._SCORM.findAPI = function (win, findAttempts, findAttemptLimit) {
  if (findAttempts > findAttemptLimit) {
    return null;
  } else if (!win.API && win.parent && win.parent !== win) {
    return document._SCORM.findAPI(win.parent, findAttempts + 1, findAttemptLimit);
  }
  return win.API;
};

document._SCORM.initializeLessonStatus = function (api) {
  var lessonStatus = api.LMSGetValue('cmi.core.lesson_status');
  if (lessonStatus === 'not attempted') {
    api.LMSSetValue('cmi.core.lesson_status', 'incomplete');
    api.LMSCommit('');
  }
};

document._SCORM.finish = function () {
  var api = document._SCORM.API;
  if (api) {
    api.LMSSetValue('cmi.core.exit', 'suspend');

    var hasCommitedChanges = api.LMSCommit('');
    if (hasCommitedChanges === 'true') {
      var hasFinishedWithLMS = api.LMSFinish('');
      if (hasFinishedWithLMS === 'true') {
        document._SCORM.API = null;
      }
    }
  }
};
