document._SessionTimer = {};
document._SessionTimer.date = new Date();

document._SessionTimer.start = function (api) {
  document._SessionTimer.end();
  document._SessionTimer.timer = setInterval(function () {document._SessionTimer.set(api); }, 5000);
};

document._SessionTimer.set = function (api) {
  if (api) {
    api.LMSSetValue('cmi.core.session_time', document._SessionTimer.timespan());
  } else {
    document._SessionTimer.end();
  }
};

document._SessionTimer.timespan = function () {
  var obj = {};
  obj._milliseconds = (new Date()).valueOf() - document._SessionTimer.date.valueOf();
  obj.milliseconds = obj._milliseconds % 1000;
  obj._seconds = (obj._milliseconds - obj.milliseconds) / 1000;
  obj.seconds = obj._seconds % 60;
  obj._minutes = (obj._seconds - obj.seconds) / 60;
  obj.minutes = obj._minutes % 60;
  obj._hours = (obj._minutes - obj.minutes) / 60;
  obj.hours = obj._hours > 10000 ? 9999 : obj._hours;
  return ('0000' + obj.hours).substr(-4) + ':' + ('00' + obj.minutes).substr(-2) + ':' + ('00' + obj.seconds).substr(-2);
};

document._SessionTimer.end = function() {
  if (document._SessionTimer.timer) {
    clearInterval(document._SessionTimer.timer);
  }
};
